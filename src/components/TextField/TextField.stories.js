//@vendors
import React from 'react'
//@components
import TextField from '.'

export default {
  title: 'TextField',
  component: TextField,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultButton = () => <TextField>datos</TextField>
