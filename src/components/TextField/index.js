//@vendors
import React from 'react'
import styled from 'styled-components'

const FormGroup = styled.div`
  padding: 0;
`

const Label = styled.div`
  color: ${(props) => props.theme.colors.blue2};
  font-weight: bold;
`

const Input = styled.input`
  border-radius: 5px;
  padding: 0 10px 0 10px;
  margin: 5px 0 20px 0;
  width: 340px;
  height: 40px;
  border: 0;
  color: ${(props) => props.theme.colors.red};
  font-weight: bold;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.blue2};
`

const TextField = ({ children, type = 'text', ...props }) => {
  return (
    <FormGroup>
      <Label>{children}</Label>
      <Input placeholder={`Ingrese ${children}`} type={type} {...props} />
    </FormGroup>
  )
}

export default TextField
