//@vendors
import React, { useState, useEffect } from 'react'
import { Formik, Field } from 'formik'
import { useHistory } from 'react-router-dom'
import * as Yup from 'yup'
//@components
import { Siguiente } from '../../components/Button'
import { RadioButton, RadioButtonGroup } from '../../components/RadioButton'
//@utilities
import { useProgressInfo } from '../../utilities/useProgressInfo'
import { insertDataEncuestaStore } from '../../utilities/validarDataStore'

export default ({
  opciones,
  pregunta,
  index,
  obtainCurrent,
  selected,
  last,
  max
}) => {
  const [opcionSeleccionada, setOpcionSeleccionada] = useState()
  const useProgress = useProgressInfo() //context
  const history = useHistory()
  useEffect(() => useProgress.finalizarEncuesta(false), [])

  const respuestaJson = (opcion) => {
    return JSON.stringify({
      index: index,
      pregunta: pregunta,
      nombreOpcion: opcion.nombreOpcion,
      correcto: opcion.esVerdadera
    })
  }

  const submitFunction = (valor) => {
    if (!last && !useProgress.isFinish) {
      obtainCurrent(index + 1)
      return useProgress.agregarResultado(valor)
    }
    if (last && !useProgress.isFinish) {
      useProgress.finalizarEncuesta()
      insertDataEncuestaStore(useProgress.resultado.concat(valor))
      return useProgress.agregarResultado(valor)
    }
    if (useProgress.isFinish) {
      history.push('/resultado')
    }
  }

  const currentCheck = (opcion) => opcionSeleccionada === respuestaJson(opcion)

  const validationSchema = Yup.object().shape({
    [pregunta]: Yup.string().required('Debe seleccionar una opción')
  })

  const initialValues = {
    [pregunta]: ''
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values, actions) => {
        setOpcionSeleccionada(values[pregunta])
        setTimeout(() => {
          submitFunction(JSON.parse(values[pregunta]))
        }, 500)
      }}
      render={({ handleSubmit, values, errors, touched, isSubmitting }) => (
        <form onSubmit={handleSubmit}>
          <RadioButtonGroup
            id={pregunta}
            label="Seleccione una opción por favor"
            value={values[pregunta]}
            error={errors[pregunta]}
            touched={touched[pregunta]}
            NextButton={
              <Siguiente
                color={
                  useProgress.isFinish && last
                    ? '#00d49d'
                    : selected
                    ? '#FF6222'
                    : null
                }
                type={'submit'}
                disabled={!selected}
              >
                {useProgress.isFinish && last
                  ? 'Ver resultado'
                  : last
                  ? 'Finalizar'
                  : 'Siguiente'}
              </Siguiente>
            }
          >
            {opciones.map((opcion, index) => (
              <Field
                key={index}
                opcion={opcion}
                disabled={!selected || useProgress.isFinish}
                component={RadioButton}
                name={pregunta}
                id={respuestaJson(opcion)}
                label={opcion.nombreOpcion}
                value={respuestaJson(opcion)}
                currentCheck={currentCheck(opcion)}
              />
            ))}
          </RadioButtonGroup>
        </form>
      )}
    />
  )
}
