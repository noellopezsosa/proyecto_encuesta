//@vendors
import React from 'react'
//@components
import RadioOptions from '.'

export default {
  title: 'Radio Options',
  component: RadioOptions,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultRadioOptions = () => (
  <RadioOptions
    selected={true}
    opciones={[
      {
        nombreOpcion: '12',
        esVerdadera: true
      },
      {
        nombreOpcion: '9',
        esVerdadera: false
      }
    ]}
  />
)
