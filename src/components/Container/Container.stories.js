//@vendors
import React from 'react'
//@components
import Container from '.'

export default {
  title: 'Container',
  component: Container,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultContainer = () => (
  <Container titulo="Título del contenedor">Contenido del contenedor</Container>
)
