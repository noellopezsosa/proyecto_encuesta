import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  padding: 0;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.gray};
  border-radius: 10px;
  margin: 5px;
  width: 100%;
  height: auto;
`

const Titulo = styled.div`
  color: ${(props) => props.theme.colors.black};
  font-weight: bold;
  text-align: 'center';
  padding: 10px;
  background: ${(props) => props.theme.colors.blue2};
  border-radius: 10px 10px 0 0;
  font-size: 17px;
  border: solid 1px gray;
`

const Contenido = styled.div`
  color: ${(props) => props.theme.colors.black};
  background: white;
  padding: 12px;
  text-align: justify;
  border-radius: 0 0 10px 10px;
  font-size: 16px;
`

export default ({ titulo, children, color }) => {
  return (
    <Wrapper>
      <Titulo style={color ? { backgroundColor: color } : {}}>{titulo}</Titulo>
      <Contenido>{children}</Contenido>
    </Wrapper>
  )
}
