//@vendors
import React from 'react'
import styled from 'styled-components'

const FormGroup = styled.div`
  margin: 5px 20px 0 0;
  text-align: right;
`

const Label = styled.label`
  color: ${(props) => props.theme.colors.blue2};
  font-style: italic;
`

const InfoCursive = ({ children }) => {
  return (
    <FormGroup>
      <Label>{children}</Label>
    </FormGroup>
  )
}

export default InfoCursive
