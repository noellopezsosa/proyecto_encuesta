//@vendors
import React from 'react'
//@components
import InfoCursive from '.'

export default {
  title: 'Texto cursivo',
  component: InfoCursive,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultButton = () => (
  <InfoCursive>Texto cursivo alineado a la derecha</InfoCursive>
)
