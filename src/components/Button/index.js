//@vendors
import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
  background: ${(props) => props.theme.colors.blue2};
  border-radius: 5px;
  width: 360px;
  height: 40px;
  color: white;
  border: 0;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.green};
`

const StyledButton3 = styled.button`
  background: ${(props) => props.theme.colors.gray};
  border-radius: 5px;
  width: 100px;
  height: 30px;
  color: black;
  border: 0;
  font-weight: bold;
  float: right;
  position: inherit;
  margin-bottom: -5px;
  margin-right: -8px;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.green};
`

const StyledButton2 = styled.button`
  background: ${(props) => props.theme.colors.red};
  border-radius: 5px;
  width: 100px;
  height: 40px;
  color: black;
  border: 0;
  font-weight: bold;
  margin-top: 20px;
  float: right;
  position: relative;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.green};
`

const StyledButton4 = styled.button`
  background: ${(props) => props.theme.colors.blue2};
  border-radius: 5px;
  width: auto;
  height: 40px;
  color: black;
  border: 0;
  font-weight: bold;
  margin-top: 20px;
  float: right;
  position: relative;
  box-shadow: inset 0px -2px 3px 0 ${(props) => props.theme.colors.green};
`

export const Siguiente = ({ children, color, ...props }) => {
  return (
    <StyledButton3 style={{ backgroundColor: color }} {...props}>
      {children}
    </StyledButton3>
  )
}

export const Salir = ({ children, ...props }) => {
  return <StyledButton2 {...props}>{children}</StyledButton2>
}

export const Nuevo = ({ children, ...props }) => {
  return <StyledButton4 {...props}>{children}</StyledButton4>
}

const Button = ({ children, type = 'submit', ...props }) => {
  return (
    <StyledButton type={type} {...props}>
      {children}
    </StyledButton>
  )
}

export default Button
