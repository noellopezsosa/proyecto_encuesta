//@vendors
import React from 'react'
//@components
import Button from '.'

export default {
  title: 'Button',
  component: Button,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultButton = () => <Button>Boton</Button>
