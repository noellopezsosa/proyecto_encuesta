//@vendors
import React from 'react'
//@components
import Checkbox from '.'

export default {
  title: 'Checkbox',
  component: Checkbox,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultCheckbox = () => <Checkbox>Selecciona</Checkbox>
