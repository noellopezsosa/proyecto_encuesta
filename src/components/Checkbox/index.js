//@vendors
import React from 'react'
import styled from 'styled-components'

const FormGroup = styled.div`
  margin-bottom: 7px;
`

const Label = styled.label`
  color: ${(props) => props.theme.colors.blue2};
  font-weight: bold;
  margin-left: 10px;
`

const Input = styled.input`
  border-radius: 3px;
  border: 0;
  white-space: nowrap;
  color: ${(props) => props.theme.colors.red};
`

const CheckBox = ({ children, id = 'checkbox1' }) => {
  return (
    <FormGroup>
      <Input type="checkbox" id={id} />
      <Label htmlFor={id}>{children}</Label>
    </FormGroup>
  )
}

export default CheckBox
