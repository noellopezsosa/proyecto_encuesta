import React from 'react'

const InputFeedback = ({ error }) => (error ? <div>{error}</div> : null)

export const RadioButton = ({
  field: { name, value, onChange },
  id,
  label,
  currentCheck,
  opcion,
  ...props
}) => {
  return (
    <div>
      <input
        name={name}
        id={id}
        type="radio"
        value={value}
        onChange={onChange}
        {...props}
      />
      <label style={currentCheck ? { color: `#FF6222` } : {}} htmlFor={value}>
        {label}
        {currentCheck
          ? opcion.esVerdadera
            ? ' - Correcta'
            : ' - Incorrecta'
          : null}
      </label>
    </div>
  )
}

export const RadioButtonGroup = ({
  error,
  touched,
  id,
  label,
  children,
  NextButton = null
}) => {
  return (
    <div>
      <fieldset>
        <legend>{label}</legend>
        {children}
        {touched && <InputFeedback error={error} />}
        {NextButton}
      </fieldset>
    </div>
  )
}
