//@vendors
import React from 'react'
//@components
import Nube from '../Nube'

export default {
  title: 'Nube',
  component: Nube,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultNube = () => <Nube color="blue" numNubes={10} />
