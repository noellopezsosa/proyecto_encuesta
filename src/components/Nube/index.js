//@vendors
import React from 'react'
//@utilities
import './styled.css'

const Nube = ({ color = null, numNubes = 1 }) => {
  const LNube = () =>
    [...Array(numNubes).keys()].map((num, index) => {
      return (
        <div
          key={index}
          id={'nube'}
          style={{
            backgroundColor: color,
            WebkitAnimationDuration: `${(num + 1) * 13}s`,
            marginTop: `${num * 130}px`
          }}
        >
          <div className="c1"></div>
          <div className="c2"></div>
          <div className="c3"></div>
        </div>
      )
    })

  return (
    <div style={{ margin: 'auto', width: '400px', textAlign: 'center' }}>
      <LNube />
    </div>
  )
}

export default Nube
