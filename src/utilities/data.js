export default [
  {
    pregunta: '¿Cuál es el operador para comparar igualdad en valor y tipo en JavaScript?',
    opciones: [
      {
        nombreOpcion: '=',
        esVerdadera: false
      },
      {
        nombreOpcion: '==',
        esVerdadera: false
      },
      {
        nombreOpcion: '===',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para convertir una cadena a un número en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'parseInt()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'toString()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'toNumber()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para agregar un elemento al final de un arreglo en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'addElement()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'push()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'append()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para eliminar un elemento del final de un arreglo en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'removeLastElement()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'pop()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'deleteLastElement()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para unir dos o más arreglos en uno solo en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'join()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'merge()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'concat()',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Cuál es la palabra clave para declarar una constante en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'const',
        esVerdadera: true
      },
      {
        nombreOpcion: 'constant',
        esVerdadera: false
      },
      {
        nombreOpcion: 'let',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para convertir un objeto a una cadena JSON en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'stringify()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'toJSON()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'parseJSON()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para convertir una cadena JSON a un objeto en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'stringify()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'toJSON()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'parse()',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Cuál es el operador para concatenar cadenas en JavaScript?',
    opciones: [
      {
        nombreOpcion: '+',
        esVerdadera: true
      },
      {
        nombreOpcion: '&',
        esVerdadera: false
      },
      {
        nombreOpcion: '*',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para obtener el número de elementos en un arreglo en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'length()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'count()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'size()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué método se usa para iterar sobre los elementos de un arreglo en JavaScript?',
    opciones: [
      {
        nombreOpcion: 'forLoop()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'whileLoop()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'forEach()',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Cuál de las siguientes opciones es un método de arreglo que devuelve el primer elemento que cumple con una condición especificada?',
    opciones: [
      {
        nombreOpcion: 'find()',
        esVerdadera: true
      },
      {
        nombreOpcion: 'filter()',
        esVerdadera: false
      },
      {
        nombreOpcion: 'map()',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué es un componente en React?',
    opciones: [
      {
        nombreOpcion: 'Una función o clase que devuelve elementos de UI',
        esVerdadera: true
      },
      {
        nombreOpcion: 'Una función que realiza una operación matemática desde distintos lugares de la aplicación',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Un objeto que almacena datos en la nube',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué es JSX en React?',
    opciones: [
      {
        nombreOpcion: 'es una estructura moderna que incluye javascript, sino html + css',
        esVerdadera: false
      },
      {
        nombreOpcion: 'una extensión que permite ejecutar mas rápidamente la aplicación',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Una sintaxis que combina JavaScript y HTML para crear elementos de UI',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Qué es un prop en React?',
    opciones: [
      {
        nombreOpcion: 'una propiedad que se define en un archivo llamado .env en el cual se configura la aplicación',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Un objeto que permite pasar datos de un componente padre a un componente hijo',
        esVerdadera: true
      },
      {
        nombreOpcion: 'como su palabra lo dice: una propiedad que permite parametrizar la manera en que se ejecutará la aplicación',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué es el estado en React?',
    opciones: [
      {
        nombreOpcion: 'Una función que devuelve un número aleatorio según se necesite',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Un objeto que almacena datos',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Un objeto que permite almacenar datos que pueden cambiar con el tiempo',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Qué es un hook en React?',
    opciones: [
      {
        nombreOpcion: 'Una función que realiza una llamada HTTP',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Una función que permite añadir estado y otras características a componentes funcionales',
        esVerdadera: true
      },
      {
        nombreOpcion: 'son funciones especiales que se declaran en el server y pueden ser llamadas posteriormente',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué es React Router?',
    opciones: [
      {
        nombreOpcion: 'Una librería que permite agregar rutas y navegación en una aplicación de React',
        esVerdadera: true
      },
      {
        nombreOpcion: 'Una función que realiza una operación matemática dentro de las rutas, sin necesidad de agregarlas al código',
        esVerdadera: false
      },
      {
        nombreOpcion: 'una liberaría que permite ocultar nombres de las rutas de acceso',
        esVerdadera: false
      }
    ]
  },
  {
    pregunta: '¿Qué es Redux en React?',
    opciones: [
      {
        nombreOpcion: 'un manejador de estados del navegador',
        esVerdadera: false
      },
      {
        nombreOpcion: 'una librería que permite manipular archivos de un proyecto a otro',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Una librería de gestión de estado que permite compartir datos entre componentes',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Qué es el Virtual DOM de React?',
    opciones: [
      {
        nombreOpcion: 'Una técnica para optimizar el rendimiento de los componentes de React',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Una herramienta para crear interfaces de usuario de alta calidad en React',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Una representación virtual de la estructura de elementos de una aplicación de React',
        esVerdadera: true
      }
    ]
  },
  {
    pregunta: '¿Cuál es la forma correcta de hacer una petición HTTP en React?',
    opciones: [
      {
        nombreOpcion: 'Utilizando la librería fetch o una librería externa como axios.',
        esVerdadera: true
      },
      {
        nombreOpcion: 'Utilizando la librería http de React.',
        esVerdadera: false
      },
      {
        nombreOpcion: 'Utilizando el objeto XMLHttpRequest de JavaScript.',
        esVerdadera: false
      }
    ]
  }
]


