//@vendors
import React, { useContext, useState, createContext } from 'react'
//@utilities
import { existDataStorage, insertDataUserStore } from './validarDataStore'
import { usernamePasswordServer } from './apiAuth'
import { Redirect } from 'react-router-dom'

const Context = createContext({})

export const AuthProvider = (props) => {
  const sesion = existDataStorage()
  const [isAuthenticated, setIsAuthenticated] = useState(sesion)

  const login = async (user, callback) => {
    const res = await usernamePasswordServer(user)

    if (res){//res.data.response) {
      insertDataUserStore(user)
      setIsAuthenticated(true)
      callback()
    }
  }

  const logout = ({ callback = () => <Redirect to="/login" /> }) => {
    insertDataUserStore({ username: '', password: '' })
    setIsAuthenticated(false)
    callback()
  }

  return (
    <Context.Provider value={{ isAuthenticated, login, logout }}>
      {props.children}
    </Context.Provider>
  )
}

export const useAuthData = () => {
  return useContext(Context)
}
