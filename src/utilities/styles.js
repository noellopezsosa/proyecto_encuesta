//@vendors
import styled from 'styled-components'

export const H3 = styled.h3`
  color: ${(props) => props.theme.colors.red};
  font-weight: bold;
  font-family: 'Arial';
`
export const H1 = styled.h1`
  color: ${(props) => props.theme.colors.blue2};
  font-weight: bold;
  font-family: 'Arial';
`

export const WrapperForm = styled.form`
  background: ${(props) => props.theme.colors.yellow};
  box-shadow: 0px 0px 15px 15px ${(props) => props.theme.colors.gray}70;
  margin: auto;
  padding: 10px;
  width: 370px;
  height: 400px;
  text-align: center;
  border-radius: 10px;
`
