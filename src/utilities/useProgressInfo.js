//@vendors
import React, { useContext, useState, createContext } from 'react'

const Context = createContext({})

export const ProgressProvider = (props) => {
  const [isFinish, setIsFinish] = useState(false)
  const [resultado, setResultado] = useState([])

  const finalizarEncuesta = (respuesta = true) => {
    setIsFinish(respuesta)
  }

  const agregarResultado = (valor) => {
    setResultado((resultado) => resultado.concat(valor))
  }

  return (
    <Context.Provider
      value={{ isFinish, finalizarEncuesta, resultado, agregarResultado }}
    >
      {props.children}
    </Context.Provider>
  )
}

export const useProgressInfo = () => {
  return useContext(Context)
}
