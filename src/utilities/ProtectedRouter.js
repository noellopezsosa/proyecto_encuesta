//@vendors
import React from 'react'
import { Route, Redirect } from 'react-router-dom'
//@utilities
import { useAuthData } from '../utilities/useAuthData'

export default ({
  children,
  validation = useAuthData().isAuthenticated,
  redirect = '/login',
  ...rest
}) => {
  return (
    <Route {...rest}>
      {validation ? children : <Redirect to={redirect} />}
    </Route>
  )
}
