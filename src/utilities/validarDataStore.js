const username = localStorage.getItem('username')
const password = localStorage.getItem('password')

export const insertDataUserStore = (data) => {
  localStorage.setItem('username', data.username)
  localStorage.setItem('password', data.password)
}

export const existDataStorage = () => {
  return !!(username && password)
}

//encuesta
export const insertDataEncuestaStore = (data) => {
  localStorage.setItem('encuesta', '')
  localStorage.setItem(
    'encuesta',
    typeof data === 'string' ? data : JSON.stringify(data)
  )
}

export const verResultadoEncuestaStore = () => {
  const encuesta = localStorage.getItem('encuesta')
  return JSON.parse(encuesta)
}
