//@vendors
import styled from 'styled-components'
//@utilities
import arbol from './img/arbol.png'

export const BackImage = styled.div`
  background: url(${arbol}) repeat-x bottom,
    linear-gradient(
      to bottom,
      ${(props) => props.theme.colors.blue2} 0%,
      ${(props) => props.theme.colors.blue1} 35%,
      ${(props) => props.theme.colors.yellow} 100%
    );
  padding: 8%;
`
