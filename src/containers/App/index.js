// @vendors
import React from 'react'
import { Switch, Route } from 'react-router-dom'
// @components
import Login from '../Login'
import Home from '../Home'
import Resultado from '../Resultado'
import { Nube } from '../../components'
// @utilities
import { BackImage } from './styles'
import { useAuthData } from '../../utilities/useAuthData'
import ProtectedRouter from '../../utilities/ProtectedRouter'
import { useProgressInfo } from '../../utilities/useProgressInfo'

export default () => {
  const auth = useAuthData()
  const progressInfo = useProgressInfo()
  return (
    <BackImage>
      {!auth.isAuthenticated && window.location.pathname === '/login' && (
        <Nube />
      )}
      <Switch>
        <ProtectedRouter path="/" exact>
          <Home />
        </ProtectedRouter>
        <ProtectedRouter
          validation={progressInfo.isFinish && auth.isAuthenticated}
          redirect="/"
          path="/resultado"
        >
          <Resultado />
        </ProtectedRouter>
        <Route path="/login" component={Login} />
      </Switch>
    </BackImage>
  )
}
