//@vendors
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
//@components
import { Button, TextField, CheckBox, InfoCursive } from '../../components'
//@utilities
import { H1, H3, WrapperForm } from '../../utilities/styles'
import { useAuthData } from '../../utilities/useAuthData'

export default () => {
  const history = useHistory()
  const auth = useAuthData()
  if (auth.isAuthenticated) return <Redirect to="/" />

  const initialValues = { username: '', password: '' }
  const [values, setValues] = useState(initialValues)

  const handleChange = (event) => {
    const { value, name } = event.target
    setValues({ ...values, [name]: value })
  }

  const onSubmit = (event) => {
    event.preventDefault()
    return auth.login(values, () => history.push('/'))
  }

  return (
    <WrapperForm onSubmit={onSubmit}>
      <H1>encuestanica.com</H1>
      <H3>REGISTRATE</H3>
      <TextField
        id="username"
        onChange={handleChange}
        name="username"
        value={values.username}
        required
      >
        Usuario
      </TextField>
      <TextField
        id="password"
        onChange={handleChange}
        name="password"
        value={values.password}
        type="password"
        required
      >
        Contraseña
      </TextField>
      <CheckBox>Recordarme</CheckBox>
      <Button type="submit" value="Entrar">
        Entrar
      </Button>
      <InfoCursive>
        Olvidó su <a href="/">contraseña?</a>
      </InfoCursive>
    </WrapperForm>
  )
}
