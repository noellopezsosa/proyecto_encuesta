//@vendors
import React, { useState, Fragment } from 'react'
//@components
import { Container, RadioOptions } from '../../components'
import { Salir } from '../../components/Button'
//@utilities
import { data } from '../../utilities'
import { useAuthData } from '../../utilities/useAuthData'
import { H3 } from '../../utilities/styles'

export default () => {
  const auth = useAuthData()
  const [currentIndex, setCurrentIndex] = useState(0)
  const obtainCurrent = (index) => setCurrentIndex(index)

  return (
    <>
      <Salir onClick={auth.logout}>Salir</Salir>
      <H3>encuestanica.com</H3>
      {data.map((pregunta, index) => {
        return (
          <Fragment key={index}>
            <Container
              color={currentIndex === index && `#FF6222`}
              titulo={pregunta.pregunta}
            >
              <RadioOptions
                opciones={pregunta.opciones}
                pregunta={pregunta.pregunta}
                index={index}
                obtainCurrent={obtainCurrent}
                selected={currentIndex === index}
                last={data.length === index + 1}
                max={data.length - 1}
              />
            </Container>
          </Fragment>
        )
      })}
    </>
  )
}
