//@vendors
import React from 'react'
import { useHistory } from 'react-router-dom'
//@components
import Container from '../../components/Container'
import { Salir, Nuevo } from '../../components/Button'
//@utilities
import { useAuthData } from '../../utilities/useAuthData'
import {
  insertDataEncuestaStore
} from '../../utilities/validarDataStore'

export default () => {
  const auth = useAuthData()
  const history = useHistory()
  const encuesta = JSON.parse(localStorage.getItem('encuesta'))
  return (
    <div>
      <Salir onClick={auth.logout}>Salir</Salir>
      <Nuevo
        onClick={() => {
          insertDataEncuestaStore('')
          history.push('/')
        }}
      >
        Nueva encuesta
      </Nuevo>
      <br />
      <br />
      <br />
      <Container titulo="RESULTADO DE LA ENCUESTA">
        {encuesta &&
          encuesta.map((dato, index) => (
            <Container key={index} color={'#fffEEE'} titulo={dato.pregunta}>
              <div style={{ fontWeight: 'bold' }}>Respuesta:</div>
              {`${dato.nombreOpcion}`}
              <div style={!dato.correcto ? { color: 'red' } : {}}>
                {`${
                  !dato.correcto
                    ? 'Tu respuesta no fué correcta'
                    : ':) Respuesta correcta'
                }`}
              </div>
            </Container>
          ))}
      </Container>
    </div>
  )
}
