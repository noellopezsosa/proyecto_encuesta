export default {
  colors: {
    blue1: '#5fbfff',
    blue2: '#00b0ff',
    green: '#00d49d',
    yellow: '#fbffc8',
    red: '#f96737',
    gray: '#9dadbc',
    white: '#dce5db',
    black: '#111',
    orange: '#FF6222'
  }
}
